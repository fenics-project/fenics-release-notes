====================
FEnicS Release Notes
====================

Release notes for major FEniCS releases, intended for publication in
Archive of Numerical Software.

Instructions
============

* Create a new branch from master:

    git branch -b v1.5

* Use template found in template/template as a starting point:

    cp -r template/template v1.5
    git add v1.5

* Write release notes in version branch.

* When ready, merge into master and submit.
