\documentclass{ansarticle}

\title{The FEniCS Project Version 1.5}
\author[1]{Martin Aln\ae{}s}
\author[2]{Jan Blechta}
\author[3]{Johan Hake}
\author[4]{Benjamin Kehlet}
\author[5]{Anders Logg}
\author[6]{Chris Richardsson}
\author[7]{Johannes Ring}
\author[8]{Marie Rognes}
\author[9]{Garth N. Wells}

\affil[1]{Simula Research Laboratory}
\affil[2]{Charles University in Prague}
\affil[3]{Simula Research Laboratory}
\affil[4]{Simula Research Laboratory}
\affil[5]{Chalmers University of Technology}
\affil[6]{University of Cambridge}
\affil[7]{Simula Research Laboratory}
\affil[8]{Simula Research Laboratory}
\affil[9]{University of Cambridge}

\runningtitle{The FEniCS Project Version 1.5}
\runningauthor{}

%------------------------------------------------------------------------------
\begin{document}

\fixme{After the release of 1.5, this document should be updated to
  include things that may be useful to include as a template for
  future releases.}

\maketitle

\begin{abstract}
  The FEniCS Project is a collaborative project for the development of
  innovative concepts and tools for automated scientific computing,
  with a particular focus on automated solution of differential
  equations by finite element methods. The FEniCS software consists of
  a collection of interoperable components, including DOLFIN, FFC,
  FIAT, Instant, UFC, UFL, and mshr. This note describes the most
  important new features introduced in the release of FEniCS version
  1.5.
\end{abstract}

\fixme{Decide who should be listed as an author of this paper. The
  list of authors should include those that have made \emph{significant}
  contributions to the release.}

%------------------------------------------------------------------------------
\section{Overview}


%------------------------------------------------------------------------------
\section{New features}


%------------------------------------------------------------------------------
\section{Interface changes}

%------------------------------------------------------------------------------
\section{How to cite FEniCS 1.5}


%------------------------------------------------------------------------------
\section{Acknowledgments}

%------------------------------------------------------------------------------
\section{[Usage instructions for this template]}

\fixme{This part should obviously be deleted when preparing the document.}

\subsection{Building the document}

Use this file (\emp{paper.tex}) as a template for your article. Then
simply type \emp{make} to build your article. This will call
\emp{pdflatex} to generate the PDF file \emp{paper.pdf}. To also call
\emp{bibtex}, type \emp{make final}.

\subsection{Typesetting mathematical formul\ae}

The ANS document class uses the \emp{amsmath} and \emp{amssymb}
packages for improved typesetting of mathematical formul\ae like,
e.g., this one:
\begin{equation}
  \cfrac{1}{\sqrt{2}+
    \cfrac{1}{\sqrt{2}+
      \cfrac{1}{\sqrt{2}+\dotsb
  }}}.
\end{equation}

Make sure that you familiarize yourself with the \emp{amsmath}
environments \emp{multline}, \emp{align} and \emp{split} (and know the
differences between them). Avoid home-made splitting using
\emp{\{array\}\{rcl\}}. For details, refer to the \emp{amsmath}
documentation.

\subsection{Typesetting tables}

Use the commands provided by the \emp{booktabs} package for better
looking typesetting of tables, as illustrated in
Table~\ref{tab:tableexample}.

\begin{table}
  \begin{center}
    \begin{tabular}{lcc}
      \toprule
      Entity & Dimension & Codimension \\
      \hline
      Vertex & $0$ & $D$ \\
      Edge & $1$ & $D-1$ \\
      Face & $2$ & $D-2$ \\
      & & \\
      Facet & $D-1$ & $1$ \\
      Cell & $D$ & $0$ \\
      \bottomrule
    \end{tabular}
  \end{center}
    \caption{Mesh entities and their dimensions/codimensions.}
    \label{tab:tableexample}
\end{table}

\subsection{Typesetting algorithms}

Use the functionality provided by the packages \emp{algorithm},
\emp{algorithmic} and \emp{algpseudocode}, automatically included by
the \emp{ansarticle} document class, to typeset algorithms. This is
illustrated in Algorithm~\ref{alg:algorithmexample}.

\begin{algorithm}
  \begin{algorithmic}
    \State $D^{0,0}(x,y) := 1$
    \State $D^{1,0}(x,y) := \frac{1+2x+y}{2}$
    \For{$p \gets 1$, $d-1$}
    \State $D^{p+1,0}(x,y) := \left( \frac{2p+1}{p+1} \right)
    \left( \frac{1 + 2x + y}{2} \right) D^{p,0}(x,y)
    - \left( \frac{p}{p+1} \right) \left( \frac{1-y}{2} \right)^2
    D^{p-1,0}(x,y)$
    \EndFor
    \For{$p \gets 0,d-1$}
    \State $D^{p,1}(x,y) := D^{p,0}(x,y) \left( \frac{1+2p+(3+2p) y}{2} \right)$
    \EndFor
    \For{$p \gets 0,d-1$}
    \For{$q \gets 1,d-p-1$}
    \State $D^{p,q+1}(x,y) :=
    \left( a_{q}^{2p+1,0} y + b_q^{2p+1,0} \right) D^{p,q}(x,y)
    - c_q^{2p+1,0} D^{p,q-1}(x,y)$
    \EndFor
    \EndFor
  \end{algorithmic}
  \caption{Compute all triangular orthogonal polynomials up to degree
    $d$ by recurrence}
  \label{alg:algorithmexample}
\end{algorithm}

\subsection{Typesetting code}

All code listings are based on the \emp{listings}
package~\citep{HeinzMoses07}. The package \emp{anslistings},
automatically included by \emp{ansarticle} document class, provides
standard styles for a variety of common programming
languages. Changing any style parameters for this package is not
encouraged and may lead to unexpected outcomes in the final
publication.

Below, we provide samples of listings for the some of the languages
available. If a language is missing, please contact the managing
editor for an updated style file.

\subsubsection{C++}

Code should be compliant with the current C++ standard. Two commands
are provided to prettyprint C++ code. First, an environment to put C++
code into the LaTeX file, namely

\begin{latexcode}
\begin{c++}
  // Your C++ code here
\end{c++}
\end{latexcode}

Alternatively, it is possible to use
\lstinline[language=TeX]!\inputcpp{file.cc}!  to print the C++ code in
a file. Here is an example of typesetting of C++ code:

\begin{c++}
// Get dimensions of local mesh_data
const unsigned int num_local_cells = mesh_data.cell_vertices.size();
assert(global_cell_indices.size() == num_local_cells);
const unsigned int num_cell_vertices = mesh_data.cell_vertices[0].size();

// Build array of cell-vertex connectivity and partition vector
std::vector<unsigned int> cell_vertices;
std::vector<unsigned int> cell_vertices_partition;
const unsigned int size = num_local_cells*(num_cell_vertices + 1);
cell_vertices.reserve(size);
cell_vertices_partition.reserve(size);
for (unsigned int i = 0; i < num_local_cells; i++)
{
  cell_vertices.push_back(global_cell_indices[i]);
  cell_vertices_partition.push_back(cell_partition[i]);
  for (unsigned int j = 0; j < num_cell_vertices; j++)
  {
    cell_vertices.push_back(mesh_data.cell_vertices[i][j]);
    cell_vertices_partition.push_back(cell_partition[i]);
  }
}

// Distribute cell-vertex connectivity
MPI::distribute(cell_vertices, cell_vertices_partition);
assert(cell_vertices.size());
cell_vertices_partition.clear();
\end{c++}

\subsubsection{Python}

The following illustrates typesetting of Python code:

\begin{python}
# Time-stepping
t = dt
while t < T:

    # Compute tentative velocity step
    b1 = assemble(L1)
    [bc.apply(A1, b1) for bc in bcu]
    solve(A1, u1.vector(), b1, "gmres", "ilu")

    # Pressure correction
    b2 = assemble(L2)
    [bc.apply(A2, b2) for bc in bcp]
    solve(A2, p1.vector(), b2, "gmres", "amg_hypre")

    # Velocity correction
    b3 = assemble(L3)
    [bc.apply(A3, b3) for bc in bcu]
    solve(A3, u1.vector(), b3, "gmres", "ilu")

    # Plot solution
    plot(p1, title="Pressure", rescale=True)
    plot(u1, title="Velocity", rescale=True)

    # Save to file
    ufile << u1
    pfile << p1

    # Move to next time step
    u0.assign(u1)
    t += dt
\end{python}

\subsection{Handling supplementary material}

Additional material which is not part of the article text, but part of
the submission, for instance setup scripts or video output, can be
linked from the article, using the \emp{hyperref} commands
(\emp{hyperref} is automaticly included by
\emp{ansarticle}). URLs to such material should start with the
filename. A suitable prefix will be added to the links in the
editorial process.

\subsection{Citations}

The \emp{ansarticle} document class imports the \emp{natbib} package,
which provides the commands \emp{\textbackslash{}citet\{\}} and
\emp{\textbackslash{}citep\{\}}. These should be used in place of the
regular \emp{\textbackslash{}cite\{\}} command as illustrated by the
following passage: It was found by \citet{Wittgenstein1921} that what
we cannot speak about we must pass over in silence, contrary to common
belief~\citep{Newton1687}.

\subsection{Other}

The \emp{ansarticle} document class provides a number of useful
notation macros which you can find by browsing through the file
\emp{ansarticle.cls}.

\subsection{Checklist}

Before submitting your manuscript to ANS, please review the following
checklist:
\begin{enumerate}
\item
  Have you run a spell checker on your manuscript (using an American English
  dictionary)?
\item
  Have you referenced all libraries you have been using and the
  appropriate versions and descriptive publications?
\item
  Have you referred to all figures and tables (callouts)?
\item
  Have you used a scalable vector graphics format for all images where
  possible?
\end{enumerate}

Before submitting your source code to ANS, please review the following
checklist:
\begin{enumerate}
\item Does a simple 'make' or similar build and run the program in a
  default configuration?
\item Did you include all configuration information necessary?
\item Did you include instructions on how to reproduce the examples in
  the text?
\end{enumerate}

%------------------------------------------------------------------------------
\bibliographystyle{abbrvnat}
\bibliography{submitting}
\end{document}
