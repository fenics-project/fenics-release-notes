[Public comments from Wolfgang]

1/ I think it would be useful to show the mesh that results from the CSG
sample code somewhere. I tried to figure out how it's going to look, and even
tried to match it with one of the meshes in Fig. 2 (which obviously doesn't
work). I imagine others may try the same and would appreciate a picture.
Alternatively, show the code for (one of) the two meshes that are shown in Fig. 2.

2/ The mini-version of the Periodic Table is reasonably high resolution, but
it cannot be zoomed in to actually read much of the text. That may or may not
be important as people could just get the original, but if you happen to have
the original as a scalable vector graphic and manage to include it here, that
would make for a nice feature.
