\documentclass{ansarticle}

\usepackage[utf8]{inputenc}
\usepackage{booktabs}

\newcommand{\fenicsversion}{1.5}

\title{The FEniCS Project Version \fenicsversion{}}
\author[1]{Martin S. Aln\ae{}s}   % http://orcid.org/0000-0001-5479-5855
\author[2]{Jan Blechta}        %
\author[3]{Johan Hake}         %
\author[4]{August Johansson}   %
\author[5]{Benjamin Kehlet}    % http://orcid.org/0000-0002-2570-0527
\author[6]{Anders Logg}        % http://orcid.org/0000-0002-1547-4773
\author[7]{Chris Richardson}   %
\author[8]{Johannes Ring}      % http://orcid.org/0000-0001-5346-9310
\author[9]{Marie E. Rognes}    %
\author[10]{Garth N. Wells}    % http://orcid.org/0000-0001-5291-7951

\affil[1]{Simula Research Laboratory, \texttt{martinal@simula.no}}
\affil[2]{Charles University in Prague, \texttt{blechta@karlin.mff.cuni.cz}}
\affil[3]{Simula Research Laboratory, \texttt{hake@simula.no}}
\affil[4]{Simula Research Laboratory, \texttt{august@simula.no}}
\affil[5]{Simula Research Laboratory, \texttt{benjamik@simula.no}}
\affil[6]{Chalmers University of Technology and University of Gothenburg, \texttt{logg@chalmers.se}}
\affil[7]{University of Cambridge, \texttt{chris@bpi.cam.ac.uk}}
\affil[8]{Simula Research Laboratory, \texttt{johannr@simula.no}}
\affil[9]{Simula Research Laboratory, \texttt{meg@simula.no}}
\affil[10]{University of Cambridge, \texttt{gnw20@cam.ac.uk}}

% Subject classification: 65M60

\runningtitle{The FEniCS Project Version \fenicsversion{}}
\runningauthor{}

%------------------------------------------------------------------------------
\begin{document}
\maketitle
%------------------------------------------------------------------------------
\begin{abstract}
  The FEniCS Project is a collaborative project for the development of
  innovative concepts and tools for automated scientific computing,
  with a particular focus on the solution of differential equations by
  finite element methods. The FEniCS Project consists of a
  collection of interoperable software components, including DOLFIN,
  FFC, FIAT, Instant, mshr, and UFL. This note describes the new
  features and changes introduced in the release of FEniCS
  version~\fenicsversion{}.
\end{abstract}

%------------------------------------------------------------------------------
\section{Overview}

FEniCS version \fenicsversion{} was released on 12th~January 2015.
This article provides an overview of the new features of this release
and serves as a citable reference for FEniCS
version~\fenicsversion{}. The FEniCS software can be downloaded from
the FEniCS Project web page at \url{http://fenicsproject.org}.

The FEniCS software consists of a collection of interoperable
components. These are, in alphabetical order, DOLFIN, FFC, FIAT,
Instant, mshr, and UFL. In addition, FEniCS includes the code
generation interface component UFC, which is now technically a part of
FFC. All FEniCS components, except mshr, are licensed under the GNU
LGPL. Due to upstream dependencies, mshr is licensed under the GNU
GPL. The components are released simultaneously with the same major
and minor version numbers, making it straightforward for users to find
compatible versions of the components. The version number of packages
that may be unchanged are still incremented to avoid divergence of
version numbers.

%------------------------------------------------------------------------------
\section{New features}

%------------------------------------------------------------------------------
\subsection{Parallel computing}

The performance of parallel computations with DOLFIN continues to
be enhanced. Notable enhancements and new features in the
\fenicsversion{} release include:
%%
\begin{itemize}
  \item switch to local (process-wise) degree-of-freedom indexing; for
    more details see subsection~\ref{sec:dofmap};
  \item full support (including from Python) for problems with over
    $2^{32}$ dofs via complete support for 64-bit indices;
  \item substantial improvements in performance scaling and memory use
    for degree-of-freedom map construction;
  \item new parallel mesh refinement strategy;
  \item support for overlapping mesh `ghost' layers in parallel;
  \item interior facet integrals are now supported in parallel, which
    enables discontinuous Galerkin methods in parallel;
  \item improvements in parallel IO using HDF5.
\end{itemize}
%%
DOLFIN has been used to solve the Poisson equation with over 12
billion degrees of freedom~\citep{RichardsonWells2015} on 24576 cores
on a Cray~XC30.

Improvements have been made to decrease the load on the filesystem
during just-in-time compilation, which can be a bottleneck on large
machines. Further effort is required to address the well-known issue
of importing Python modules on parallel computers.

A memory corruption issue during forks on OFED-based (InfiniBand)
clusters has been tracked down.  Users of OFED machines are advised to
read the Instant \emp{README} file and export the
\emp{INSTANT\_SYSTEM\_CALL\_METHOD} environment variable.

%------------------------------------------------------------------------------
\subsection{New mesh refinement strategy}

A new refinement algorithm has been adopted for DOLFIN, following the
work of~\citet{PlazaCarey2000}. This algorithm uses edge bisection to
subdivide the marked triangles of a \emp{Mesh}, be they the cells
themselves in two dimensions, or the facets of tetrahedra in
three-dimensions. The subdivision is chosen so as to maximize the
internal angles of the new triangulation, thus maintaining mesh
quality after multiple refinements; see Figure~\ref{fig:refine}. Edges
which are marked for refinement propagate between processes in
parallel, whilst the refinement operation is local, resulting in good
scaling.

\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{images/refine.png}
  \caption{Tetrahedron refined multiple times, whilst preserving mesh
    quality.}
  \label{fig:refine}
\end{figure}

%------------------------------------------------------------------------------
\subsection{Mesh generation: the new \emph{mshr} component}

The mesh generation functionality of FEniCS, except for simple meshes
like \emp{UnitSquareMesh} and \emp{UnitCubeMesh}, has been moved to a
new FEniCS Component named \emph{mshr}. The motivation for this change
is twofold: first, to simplify the list of dependencies and reduce the
amount of resources (CPU time and memory usage) for building DOLFIN
(by moving the CGAL dependency to mshr); and second, to simplify the
addition of new features to the new meshing component. Currently, both
CGAL and Tetgen are available as mesh generation backends for mshr.

Much like the old meshing interface in DOLFIN, the new mshr interface
allows simple generation of meshes from Constructive Solid Geometry
(CSG) descriptions, as demonstrated by the example listed below. The
generated mesh is shown in Figure~\ref{fig:csg_meshes}.
%%
\begin{python}
from dolfin import *
from mshr import *

# Define geometry
box = Box(Point(0, 0, 0), Point(1, 1, 1))
sphere = Sphere(Point(0, 0, 0), 0.3)
cylinder = Cylinder(Point(0, 0, -1), Point(0, 0, 1), 1.0, 0.5)
geometry = box + cylinder - sphere

# Generate mesh
mesh = generate_mesh(geometry, 16)
\end{python}

\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{images/csgmesh-1.png} \quad
  \includegraphics[width=0.4\textwidth]{images/csgmesh-2.png}
  \caption{A mesh generated from a CSG description in FEniCS. The left
    image shows the surface of the generated tetrahedral mesh while the
    right image shows a volume rendering in which the subtracted
    sphere in the center is visible.}
  \label{fig:csg_meshes}
\end{figure}

Through a combination of simple CSG primitives and Boolean operations
(and, or, negation), meshes can be created for geometries ranging from
simple domains like the canonical L-shaped domain, to relatively
complex geometries. Figure~\ref{fig:meshes_mshr} shows a pair of meshes
generated using mshr.

\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{images/deathstar.png} \quad
  \includegraphics[width=0.4\textwidth]{images/classic.png}
  \caption{Meshes generated from CSG geometries using the new FEniCS
    mesh generation component mshr.}
  \label{fig:meshes_mshr}
\end{figure}

%------------------------------------------------------------------------------
\subsection{Multimesh finite element methods}

Multimesh finite element methods (CutFEM) are finite element methods
posed on two or more possibly non-matching meshes. The formulation of
multimesh finite element methods involves the formulation of
variational problems on function spaces composed by regular function
spaces on the intersecting meshes. Continuity between meshes is
imposed using Nitsche's method and stability ensured by adding
suitable stabilization terms (ghost penalties) to the variational
problem.

FEniCS \fenicsversion{} adds support for the formulation of multimesh
methods, including automatic and efficient computation of mesh-mesh
intersections, quadrature rules for cut cells and interfaces, and
expression of integrals that typically appear in the formulation of
multimesh/CutFEM methods. Figure~\ref{fig:stokes} shows results for a
Taylor--Hood formulation for the Stokes problem on two
overlapping and intersecting meshes; a boundary-fitted mesh embedding
a propeller-shaped hole, which overlaps a fixed background mesh; for
details see~\citet{JohanssonLarsson2015a}.

\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{images/propeller-and-background-mesh3.png}
  \includegraphics[width=0.4\textwidth]{images/propellerflow.png}
  \caption{Multimesh solution of the Stokes problem on two overlapping
    non-matching meshes.}
  \label{fig:stokes}
\end{figure}

The FEniCS interface to multimesh finite element methods currently
requires a user to define the integrals in terms of the newly added
\emp{custom\_integral} interface. This new interface enables code
generation for integrals expressed on arbitrary domains, among them
cut cells, interfaces, and overlaps, as illustrated by the following
UFL code for expression of a multimesh formulation of Poisson's
equation:

\begin{python}
# Define custom measures (simplify syntax in future versions)
dc0 = dc(0, metadata={"num_cells": 1})
dc1 = dc(1, metadata={"num_cells": 2})
dc2 = dc(2, metadata={"num_cells": 2})

# Define measures for integration
dx = dx + dc0 # domain integral
di = dc1      # interface integral
do = dc2      # overlap integral

# Parameters
alpha = 4.0
beta = 4.0

# Bilinear form
a = dot(grad(u), grad(v))*dx \
  - dot(avg(grad(u)), jump(v, n))*di \
  - dot(avg(grad(v)), jump(u, n))*di \
  + alpha/h*jump(u)*jump(v)*di \
  + dot(jump(grad(u)), jump(grad(v)))*do
\end{python}

On the C++ side, multimesh function spaces must currently be explicitly
constructed as demonstrated by the following excerpt from the
\emp{multimesh-poisson} demo:
%%
\begin{c++}
// Create function spaces
MultiMeshPoisson::FunctionSpace V0(mesh_0);
MultiMeshPoisson::FunctionSpace V1(mesh_1);
MultiMeshPoisson::FunctionSpace V2(mesh_2);

// Build multimesh function space
MultiMeshFunctionSpace V;
V.parameters("multimesh")["quadrature_order"] = 2;
V.add(V0);
V.add(V1);
V.add(V2);
V.build();
\end{c++}

Future improvements to the multimesh functionality of FEniCS will
include the introduction of specific integration measures in UFL, so
that the measures don't need to be defined in terms of
\emp{custom\_measure}, as well as simplified and higher level
interfaces for assembly and problem solving in DOLFIN.

%------------------------------------------------------------------------------
\subsection{Assembly of point integrals}

In addition to the classical cell integrals, exterior facet integrals,
and interior facet integrals, FEniCS version \fenicsversion{} supports
specification of \emph{point integrals}. A point integral represents
the integrand evaluated at each vertex, summed over the vertices of the
mesh. Starting with FEniCS version \fenicsversion{}, the DOLFIN
assemblers recognize point integrals and assemble contributions from
such integrals. We remark that for each vertex, the integrand will be
restricted to the arbitrarily chosen first cell associated with this
vertex prior to evaluation. This operation is therefore only
consistently defined for functions and integrands that are continuous
at the vertices.

%------------------------------------------------------------------------------
\subsection{Point integral solvers and Rush--Larsen schemes}

The FEniCS \emp{PointIntegralSolvers} are special-purpose solvers dedicated
to solving multistage integration schemes defined for each vertex of a
mesh. Such problems typically occur in connection with spatially
varying systems of ODEs, for instance in electrophysiology. In
FEniCS version~\fenicsversion{}, we have extended the range of
available schemes to also include the
Rush--Larsen \citep{RushLarsen1978} and the generalized
Rush--Larsen \citep{SundnesEtAl2009} schemes for solving systems of
nonlinear ODEs.

%------------------------------------------------------------------------------
\subsection{Redesign of core symbolic representation and algorithms}

The domain-specific language UFL represents equations as symbolic
expression trees, where each node represents some value or
mathematical operation. For nonlinear equations with large expression trees, the performance
and memory overhead of symbolic computations and form compiler
algorithms can become significant, especially when running in parallel,
where this overhead is a limiting factor for scalability according to
Amdahl's law. This becomes even more important when FEniCS is combined
with high-level algorithms, such as the dolfin-adjoint package, that
rely extensively on symbolic algorithms from UFL. To reduce this
overhead, the core representation of symbolic expressions has been
redesigned (see \emp{ufl.core}), followed by a rewrite of the core
framework for developing additional symbolic algorithms (see
\emp{ufl.corealg}). Each expression tree node type now has the following
attributes:
%%
\begin{description}
\item[$\bullet$ \emp{ufl\_operands}] Tuple of child nodes in expression
  tree. Equivalent to deprecated \emp{operands()}.
\item[$\bullet$ \emp{ufl\_shape}] Tuple of value shape dimensions. Equivalent to
  deprecated \emp{shape()}.
\item[$\bullet$ \emp{ufl\_free\_indices}] Tuple of free index integer
  ids. Replaces deprecated \emp{free\_indices()}.
\item[$\bullet$ \emp{ufl\_free\_index\_dimensions}] Tuple of dimensions
  associated with corresponding free index ids in
  \emp{ufl\_free\_indices}. Replaces deprecated \emp{free\_indices()}.
\end{description}
%%
The deprecated functions mentioned above are still available as
wrappers around the new attributes. Type traits of symbolic expression
classes have also been made available through an efficient property-based
API using the naming scheme \emp{ExprType.\_ufl\_*\_} (see
\emp{ufl.core.expr}). This is mainly intended for internal usage but
also for developers building on the symbolic capabilities of FEniCS.

A major guiding principle in the redesign of core algorithms in UFL
was to replace all uses of expensive recursion in Python
with loops. Algorithms for expression traversal (see module
\emp{ufl.corealg.traversal}) have all been reimplemented without
recursion, and further fine-tuned for specific iteration scenarios. The
former \emp{Transformer} base class for visitor-like algorithms has
been deprecated in favor of an implementation that avoids
recursion. The new function \emp{map\_expr\_dag(function, expression)}
applies the given function to transform each expression tree node in a
non-recursive post-order traversal. The function arguments include the
node as well as the previously transformed node operands. This
algorithm is further optimized to reduce the number of function calls
and avoid duplicated expression tree nodes by (optionally) caching
intermediate results in a hashmap with the original expression nodes
as keys.

Previous implementations of the \emp{Transformer} class can easily be
rewritten as implementations of \emp{MultiFunction}, which implements
the same dynamic type dispatch mechanism, and passed as the function
argument to \emp{map\_expr\_dag}. Key algorithms such as automatic
differentiation (\emp{apply\_derivatives}) and propagation of
restrictions (\emp{apply\_restrictions}) have been reimplemented in
this optimized algorithm framework, and may serve as models.  In
addition, form signatures are now computed without first applying
automatic differentiation, which reduces the symbolic overhead
significantly when the signature is found in the form compiler cache.
Together these improvements have reduced the symbolic overhead in
FEniCS~1.5 by 20--60\%. To give a few examples: the initial
just-in-time (JIT) compilation of a Poisson equation form was reduced
from $5 s$ to $4 s$ including C++ compilation, while for a complicated
cardiac hyperelasticity model the JIT time was reduced from $103 s$ to
$48 s$. For the latter model, subsequent program runs now avoid a $2
s$ delay in the signature computation used for disk cache lookup.

%------------------------------------------------------------------------------
\subsection{Notation from Periodic Table of the Finite Elements}

FEniCS now supports the notation for finite elements as set out in the
Periodic Table of the Finite Elements~\citep{ArnoldLogg2014} based on
the finite element exterior calculus, see~\citet{Arnold:2006aa}. The
notation introduces new aliases for the existing elements in FEniCS
and introduces new placeholder names for the not yet implemented
elements on quadrilaterals and hexahedra. Earlier FEniCS notation
(``Lagrange'', ``CG'', etc.) is still supported but may be phased out
in future versions. Table~\ref{tab:periodictable} summarizes the
notation. In this table, $n$ is the topological dimension of the
finite element domain (1D, 2D, 3D), $r$ is the polynomial degree and
$k$ is the form degree; $k = 0$ means scalars, $k = 1$ means 1-forms
(oriented line segments), $k = 2$ means 2-forms (oriented surfaces)
and $k = 3$ means 3-forms (oriented volumes).

Figure~\ref{fig:periodictable} shows a miniature (but high
resolution) version of the periodic table. The full version can be
downloaded from \emp{http://femtable.org}.

\begin{table}
  \centering
  \begin{tabular}{c}
    \toprule
    $\mathcal{P}_r^-\Lambda^k(\Delta_n)$ \\
    \midrule
    \begin{tabular}{l|llll}
      & $k=0$ & $k=1$ & $k=2$ & $k=3$ \\
      \hline
      $n=1$ & \emp{P} & \emp{DP} &\\
      $n=2$ & \emp{P} & \emp{RT[E,F]} & \emp{DP} \\
      $n=3$ & \emp{P} & \emp{N1E} & \emp{N1F} & \emp{DP} \\
    \end{tabular}
  \end{tabular}
  \quad
  \begin{tabular}{c}
    \toprule
    $\mathcal{P}_r\Lambda^k(\Delta_n)$ \\
    \midrule
    \begin{tabular}{l|llll}
      & $k=0$ & $k=1$ & $k=2$ & $k=3$ \\
      \hline
      $n=1$ & \emp{P} & \emp{DP} &\\
      $n=2$ & \emp{P} & \emp{BDM[E,F]} & \emp{DP} \\
      $n=3$ & \emp{P} & \emp{N2E} & \emp{N2F} & \emp{DP} \\
    \end{tabular}
  \end{tabular}

  \bigskip

  \begin{tabular}{c}
    \toprule
    $\mathcal{Q}_r^-\Lambda^k(\square_n)$ \\
    \midrule
    \begin{tabular}{l|llll}
      & $k=0$ & $k=1$ & $k=2$ & $k=3$ \\
      \hline
      $n=1$ & \emp{Q} & \emp{DQ} &\\
      $n=2$ & \emp{Q} & \emp{RTC[E,F]} & \emp{DQ} \\
      $n=3$ & \emp{Q} & \emp{NCE} & \emp{NCF} & \emp{DQ} \\
    \end{tabular}
  \end{tabular}
  \quad
  \begin{tabular}{c}
    \toprule
    $\mathcal{S}_r\Lambda^k(\square_n)$ \\
    \midrule
    \begin{tabular}{l|llll}
      & $k=0$ & $k=1$ & $k=2$ & $k=3$ \\
      \hline
      $n=1$ & \emp{S} & \emp{DPC} &\\
      $n=2$ & \emp{S} & \emp{BDMC[E,F]} & \emp{DPC} \\
      $n=3$ & \emp{S} & \emp{AAE} & \emp{AAF} & \emp{DPC} \\
    \end{tabular}
  \end{tabular}
  \caption{New notation for finite elements adopted from the Periodic
    Table of the Finite Elements.}
  \label{tab:periodictable}
\end{table}

\begin{figure}
  \centering
  \includegraphics[angle=-90,width=\textwidth]{images/femtable.pdf}
  \caption{Periodic Table of the Finite Elements.}
  \label{fig:periodictable}
\end{figure}

As an example, the following code shows how to instantiate a pair of
cubic N\'ed\'elec face elements of the first and second kinds on a
tetrahedron:
%%
\begin{python}
element_1 = FiniteElement("N1F", "tetrahedron", 3)
element_2 = FiniteElement("N2F", "tetrahedron", 3)
\end{python}

%------------------------------------------------------------------------------
\subsection{Changes in degree-of-freedom  map construction}
\label{sec:dofmap}

A number of performance improvements have been made in the
degree-of-freedom (DOF) map construction, and changes to how to DOF
maps are order have been made. Specifically:
%
\begin{itemize}
\item DOLFIN now uses local (process-wise) numbering of degrees of
  freedom, which has reduced the memory use of degree-of-freedom
  maps. This change also permits some new developments in interfacing
  to linear-algebra backends.

\item The degree-of-freedom map construction code has been re-written,
  reducing the time for construction and substantially improving
  parallel scaling of construction.

\item Node-wise block structure in DOF maps, e.g.~a fixed number of
  DOFs at each node, is now automatically detected. This provides
  better data locality and faster graph-based DOF map reordering.
\end{itemize}
%
Improvements in sparsity patterns that follow from improved DOF map
ordering are shown in Figures~\ref{fig:gear} and~\ref{fig:naca}. The
sparsity patterns produced by DOLFIN 1.5.0 are better clustered around
the diagonal. When DOLFIN is configured with SCOTCH, the SCOTCH
implementation of Gibbs-Poole--Stockmeyer reordering is used.

The code for reproducing the sparsity patterns in
Figures~\ref{fig:gear} and~\ref{fig:naca} and patterns on other meshes
can be found at
\url{https://www.repository.cam.ac.uk/handle/1810/252670}.
The meshes are shown at
\url{http://fenicsproject.org/download/data.html#meshes}.

\graphicspath{{sparsity_pattern/data/}}
\begin{figure}
  \includegraphics[width=0.5\textwidth]{{{gear.xml.gz_dolfin-1.4.0}}}
  \includegraphics[width=0.5\textwidth]{{{gear.xml.gz_dolfin-1.5.0}}}
  \caption{Comparison of sparsity patterns of the Taylor--Hood Stokes
    operator on the DOLFIN \emp{gear} mesh.}
  \label{fig:gear}
\end{figure}
\begin{figure}
  \includegraphics[width=0.5\textwidth]{{{naca0018_3d.xml.gz_dolfin-1.4.0}}}
  \includegraphics[width=0.5\textwidth]{{{naca0018_3d.xml.gz_dolfin-1.5.0}}}
 \caption{Comparison of sparsity patterns of the Taylor--Hood Stokes
   operator on a \emp{NACA 0018 airfoil} mesh.}
  \label{fig:naca}
\end{figure}

%------------------------------------------------------------------------------
\subsection{Linear algebra interface}

FEniCS uses the PETSc and SLEPc libraries as backends providing parallel
infrastructure for linear algebra, non-linear solvers, and eigensolvers.
The wrapper layer to these packages has been improved, in particular,
as follows:
\begin{itemize}
  \item
    \emp{PETScTAOSolver}, the DOLFIN interface to the PETSc TAO framework,
    which provides solvers for non-linear optimization problems,
    has been introduced;
  \item
    \emp{LinearOperator}, the DOLFIN interface to matrix-free operators,
    can now be unwrapped into a~respective PETSc or petsc4py object;
  \item
    \emp{SLEPcEigenSolver}, the DOLFIN interface to eigensolvers,
    can now be unwrapped into a~respective SLEPc or slepc4py object.
\end{itemize}
The latter two extend the feature already available for other backend
wrappers, e.g., \emp{PETScMatrix}, \emp{PETScKrylovSolver}, etc.
This feature allows for finer, but still convenient, control over
a~solution process and other details, in such cases when the coverage of
backend features or options in DOLFIN is not sufficiently detailed.

In this release, support for the Trilinos Epetra backend has been
removed. Support for the newer Trilinos Tpetra backend will be added
to a future release.

%------------------------------------------------------------------------------
\subsection{Use of C++ 11}
\label{sec:cxx11}

The FEniCS team has started using selected C++11 features, allowing
removal of the dependency on Boost shared pointers and some
simplifications of the C++ source code.  The set of features is
limited for the time being, to remain compatible with some common
compilers. GCC 4.6.3 is known to be compatible.

For users of the DOLFIN C++ interface, the use of C++11 can lead to
less verbose code by using the key word \emp{auto}, initializer lists,
and range-based loops, for instance.

%------------------------------------------------------------------------------
\subsection{Python versions}

FEniCS now requires Python 2.7, and has experimental support for
Python 3.x. As part of supporting Python 3, FIAT no longer depends on
Scientific Python. In place of Scientific Python, which does not
support Python 3, FIAT now depends on SymPy.

%------------------------------------------------------------------------------
\subsection{Packaging and installation}

As before, FEniCS is available as a binary package as part of the
standard Debian and Ubuntu repositories. For users who wish to access
the latest FEniCS versions before these have propagated downstream, a
personal package archive (PPA) is provided
(\url{ppa:fenics-packages/fenics}). A binary package is also available
for Mac OSX.

With the release of version \fenicsversion{}, FEniCS has retired the
installer Dorsal in favor of the new tool
\emp{fenics-install.sh}. This script relies on
HashDist~(\url{https://hashdist.github.io/}) for installation of
FEniCS and its dependencies. Using this script, users can easily build
FEniCS with a one-line command:
%%
\begin{center}
  \emp{curl -s http://fenicsproject.org/fenics-install.sh | bash}
\end{center}
%%
FEniCS developers (who need to modify the source code) are encouraged
to use the two related scripts \emp{fenics-install-all.sh} and
\emp{fenics-install-component.sh} as standard tools in their normal
workflow. These scripts are part of the FEniCS Developer Tools
repository on BitBucket.

In addition, FEniCS can now also be installed via a
community-supported Conda recipe, see
\url{https://github.com/Juanlu001/fenics-recipes/releases/tag/v1.5.0}
as well as via a custom virtualization image provided on the FEniCS
web page.

%------------------------------------------------------------------------------
\section{New demo programs}

Some new example programs have been added to demonstrate the use of
new user-level features. The new demo programs are:
%
\begin{itemize}
\item Smoothed aggregation algebraic multigrid for three-dimensional
  elasticity. The demo illustrates the construction and specification
  of the near-nullspace which is required by the multigrid
  preconditioner. It also demonstrates how to construct a scalable
  solver for elasticity problems.

\item Two new demos illustrating the implementation of
  multimesh-methods: \emp{multimesh-poisson} and
  \emp{multimesh-stokes}.
\end{itemize}

%------------------------------------------------------------------------------
\section{Interface changes}

The following interface changes were made in the release of FEniCS
version \fenicsversion{}:
%%
\begin{itemize}
\item
  The classes \emp{FacetArea}, \emp{FacetNormal}, \emp{CellSize},
  \emp{CellVolume}, \emp{SpatialCoordinate}, \\ \emp{Circumradius},
  \emp{MinFacetEdgeLength}, and \emp{MaxFacetEdgeLength} now require a
  \emp{Mesh} argument instead of a \emp{Cell} argument.
\item
  The signature for the \emp{assemble} function has been simplified to
  not require/accept the arguments \emp{coefficients}, \emp{cells},
  and \emp{common\_cell}.
\item
  Differentiation of expressions with respect to a \emp{Coefficient}
  or \emp{Function} using \emp{diff} does not require the function to
  be wrapped in a \emp{variable} construct.
\item
  Quadrature degree and rule name can be specified as keywords to form
  integration measures, i.e. \emp{f*dx(degree=3, rule="canonical")}.
  The syntax \\ \emp{Measure\allowbreak .\_\_getitem\_\_\allowbreak
    (mesh\_function)\allowbreak .\_\_call\_\_\allowbreak
    (subdomain\_id)}, \\ e.g.,  \emp{dx[mesh\_function](42)}, has been
  deprecated in favor of keyword arguments, for example:
  \emp{dx(subdomain\_data=mesh\_function, \allowbreak
    subdomain\_id=42)}.
\item
  \emp{GenericTensor}s cannot be reinitialized anymore, hence the
  \emp{reset\_sparsity} parameter has been removed from all assemble
  functions.
\item
  Deprecated \emp{MPI::process\_number} and \emp{MPI::num\_processes}
  were removed in favor of \emp{MPI::rank} and \emp{MPI::size}
  respectively.
\item
  \emp{GenericVector.\_\_setitem\_\_}, e.g., \emp{vec[indices] =
    values}, now takes local \emp{indices} which can be list of ints,
  NumPy array of ints, int, or full slice \emp{:}. Type of \emp{values}
  can be scalar, NumPy array, or \emp{GenericVector} where
  applicable. Operation is collective and finalizes the vector
  automatically.
\end{itemize}

%------------------------------------------------------------------------------
\section{Dependencies}

FEniCS, mostly DOLFIN, depends on a number of external packages. Here
we list and describe these external packages, along with required
version numbers (to the best of our knowledge).

Necessary dependencies:
%%
\begin{itemize}
  \item
    C++11 compiler (GCC $\geq$ 4.6 and Clang 3.6 are known to work),
    see section~\ref{sec:cxx11} for details
  \item
    Boost $\geq$ 1.48; provides some low-level data structures and
    algorithms, DOF reordering and mesh coloring
    \citep{Schling:2011:BCL:2049814}
  \item
    CMake $\geq$ 2.8; used by DOLFIN build system and optionally by
    Python JIT chain
  \item
    Eigen $\geq$ 3.0; provides data structures and operations used by
    adaptive solvers \citep{eigenweb}
  \item
    Python 2.7 or $\geq$ 3.2; six (Python 2 and 3 compatibility tool);
    needed by FIAT, UFL, FFC, and Instant; optionally needed
    for Python interface to DOLFIN and mshr
  \item
    NumPy; Python package providing data structures and operations on
    N-dimensional arrays \citep{NumPy}
  \item
    SymPy; Python symbolic maths library, used by FIAT \citep{SymPy}
\end{itemize}
%%
Optional dependencies:
%%
\begin{itemize}
  \item
    OpenMP; supports parallel computing on shared memory systems
  \item
    MPI; supports parallel computing on distributed memory systems
  \item
    PETSc $\geq$ 3.3, $<$ 3.6; serves as parallel (linear) algebra backend;
    provides data structures and routines for handling parallel
    vectors, matrices, Krylov solvers, sparse direct solvers, various
    preconditioners and non-linear solvers \citep{petsc-web-page,
      petsc-user-ref, petsc-efficient}
  \item
    petsc4py $\geq$ 3.3, $<$ 3.6; provides Python bindings to
    PETSc~\citep{Dalcin20111124}
  \item
    SLEPc $\geq$ 3.3, $<$ 3.6; provides parallel eigen-problem
    solvers~\citep{Hernandez:2005:SSF}
  \item
    slepc4py $\geq$ 3.5.1, $<$ 3.6; provides Python bindings to SLEPc
    \citep{Dalcin20111124}
  \item
    UMFPACK; provides sequential sparse LU decomposition using
    multifrontal method~\citep{Davis:2004:AUV:992200.992206}
  \item
    CHOLMOD; provides sequential sparse Cholesky decomposition using
    supernodal method \citep{Chen:2008:ACS:1391989.1391995}
  \item
    PaStiX $\geq$ 5.2.1; provides parallel (both distributed and
    threaded) sparse LU and Cholesky
    decomposition~\citep{A:LaBRI::HRR01a}
  \item
    Trilinos $\geq$ 11.0.0; provides mesh partitioning and
    coloring~\citep{Trilinos}
  \item
    SCOTCH; provides mesh partitioning and DOF
    reordering~\citep{C:LaBRI::chepelpmaa06}
  \item
    ParMETIS $\geq$ 4.0.2; provides mesh partitioning
    ~\citep{Karypis:1998:FHQ:305219.305248}
  \item
    SWIG $\geq$ 2.0 (with Python 2) or $\geq$ 3.0.3 (with Python 3);
    tool needed for generating Python interface to DOLFIN and mshr
  \item
    flufl.lock; implements file locking (used by Instant) on NFS file system
  \item
    HDF5; provides parallel, scalable IO backend; needs to be built with
    parallel support~\citep{hdf5}
  \item
    zlib; reading compressed XML files and compressed VTK output
  \item
    VTK $\geq$ 5.2; provides interactive plotting in DOLFIN
  \item
    CGAL 4.5.1, TetGen 1.5.0; mshr backends, built automatically by
    mshr build system~\citep{cgal:eb-15a, Si:2015:TDQ:2732672.2629697}
  \item
    GMP, MPFR; arbitrary-precision and multiple-precision arithmetic libraries,
    required by mshr~\citep{GMP, Fousse:2007:MMP}
  \item
    Scipy; needed by UFL for evaluation of error and Bessel functions
  \item
    pytest; Python testing tool, needed for running unit tests
  \item
    Sphinx $\geq$ 1.1.0; enables building documentation
  \item
    Qt4; provides API for writing graphical applications
  \item
    Soya; Python 3D engine, used for plotting finite elements
\end{itemize}

%------------------------------------------------------------------------------
\section{How to cite FEniCS \fenicsversion{}}

Users of FEniCS version \fenicsversion{} are encouraged to cite this
article, in addition to one or more of the publications listed on the
FEniCS Project web site:

\begin{center}
  \url{http://fenicsproject.org/citing/}
\end{center}

%------------------------------------------------------------------------------
\section{Acknowledgments}

Although this article lists as authors only those developers who have
made significant contributions specifically to the release of
FEniCS~\fenicsversion{}, we gratefully acknowledge the contributions
by many people to the FEniCS Project, including
%%
Martin S. Alnæs,
Jan Blechta,
Juan Luis Cano Rodríguez
Patrick Farrell,
Johan Hake,
Johan Hoffman,
Johan Jansson,
Niclas Jansson,
August Johansson,
Claes Johnson,
Benjamin Kehlet,
Robert C.~Kirby,
Matthew Knepley,
Miroslav Kuchta,
Hans Petter Langtangen,
Anders Logg,
Kent-Andre Mardal,
Andre Massing,
Mikael Mortensen,
Harish Narayanan,
Chris Richardson,
Johannes Ring,
Marie E.~Rognes,
L.~Ridgway Scott,
Ola Skavhaug,
Andy Terrel,
Garth~N.~Wells, and
Kristian Ølgaard.

The FEniCS Project is supported by The Research Council of Norway
through a Centre of Excellence grant to the Center for Biomedical
Computing at Simula Research Laboratory, project number 179578.
%
Anders Logg acknowledges support by the Swedish Research Council Grant
No.\ 2014--6093.
%
Jan Blechta acknowledges the support of the project LL1202 in the
programme ERC-CZ funded by the Ministry of Education, Youth and Sports
of the Czech Republic.
%------------------------------------------------------------------------------
\bibliographystyle{abbrvnat}
\bibliography{bibliography}
%------------------------------------------------------------------------------
\end{document}
%------------------------------------------------------------------------------
