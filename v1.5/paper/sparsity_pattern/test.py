# Copyright (C) 2015 Jan Blechta, Gaspard Jankowiak
# License: GNU LGPL v3
from dolfin import *
from matplotlib import pyplot as plt
import scipy.sparse
import os, sys

parameters['linear_algebra_backend'] = 'uBLAS'
not_working_in_parallel('Plotting sparsity pattern')
info('DOLFIN ver: %s, git: %s' % (dolfin_version(), git_commit_hash()))

# Set parameters important for sparsity pattern explicitly
# Those are defaults on most system
parameters['dof_ordering_library'] = 'SCOTCH'
parameters['reorder_dofs_serial'] = True
if dolfin_version() >= '1.5.0':
    parameters['reorder_cells_gps'] = False
    parameters['reorder_vertices_gps'] = False


def plot_stokes_pattern(mesh):
    """Assembles Stokes operator on Taylor-Hood space on given 'mesh',
    plots its sparsity pattern and stores it to file given by mesh.name()
    and DOLFIN version."""
    # Assemble Taylor-Hood Stokes operator
    P2 = VectorElement('Lagrange', mesh, 2)
    P1 = FiniteElement('Lagrange', mesh, 1)
    W = FunctionSpaceBase(mesh, P2*P1)
    u, p = TrialFunctions(W)
    v, q = TestFunctions(W)
    a = ( inner(grad(u), grad(v)) - p*div(v) - q*div(u) )*dx
    A = assemble(a)

    # Plot sparsity pattern
    info("Plotting Stokes pattern on '%s', #DOFs = %d, tdim = %d, gdim = %d"
         % (mesh.name(), W.dim(), mesh.topology().dim(), mesh.geometry().dim()))
    plt.figure()
    matrix_spy(plt, A)
    plt.title(mesh.name() + ', DOLFIN %s' % dolfin_version())
    plt.draw()
    plt.savefig(mesh.name() + '_dolfin-%s.png' % dolfin_version(), dpi=300)


def matrix_spy(pyplot, M):
    """Using pyplot instance plots sparsity pattern of DOLFIN matrix."""
    rows, cols, values = M.data()
    mat = scipy.sparse.csr_matrix((values, cols, rows))
    pyplot.spy(mat, marker=',', color='k')


def walk_meshfiles(func):
    """Walks through current working directory, tries loading every file
    as DOLFIN mesh and processes the mesh by 'func'."""
    for dir, _, files in os.walk(os.getcwd()):
        for f in files:
            f = os.path.join(dir, f)

            # Try loading as DOLFIN mesh
            try:
                mesh = Mesh(f)
            except RuntimeError:
                continue

            # Rename by filename
            name = os.path.basename(f)
            mesh.rename(name, name)

            # Plot mesh
            p = plot(mesh)
            p.write_png(f)

            # Process
            func(mesh)


if __name__ == '__main__':

    # Enable plotting on screen if not disabled by DOLFIN_NOPLOT env var
    if not os.environ.get('DOLFIN_NOPLOT'):
        plt.ion()

    # Process all meshes in data/ dir
    os.chdir('data/')
    #walk_meshfiles(plot_stokes_pattern)

    # Process selected meshes for paper
    files = [
        'aneurysm.xml.gz',
        'dolfin_fine.xml.gz',
        'gear.xml.gz',
        'intersecting_surfaces.xml.gz',
        'naca0018_3d.xml.gz',
        'pulley.xml.gz',
    ]
    for f in files:
        mesh = Mesh(f)
        mesh.rename(f, f)
        p = plot(mesh)
        p.write_png(f)
        #interactive() # Tweak the plot manually
        plot_stokes_pattern(mesh)
