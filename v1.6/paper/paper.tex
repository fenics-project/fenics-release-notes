\documentclass{ansarticle}

\usepackage[utf8]{inputenc}
\usepackage{booktabs}

\newcommand{\fenicsversion}{1.6}

\title{The FEniCS Project Version \fenicsversion{}}
\author[1]{Martin S. Aln\ae{}s}   % http://orcid.org/0000-0001-5479-5855
\author[2]{Jan Blechta}        %
\author[3]{Johan Hake}         %
\author[4]{August Johansson}   %
\author[5]{Benjamin Kehlet}    % http://orcid.org/0000-0002-2570-0527
\author[6]{Anders Logg}        % http://orcid.org/0000-0002-1547-4773
\author[7]{Chris Richardson}   %
\author[8]{Johannes Ring}      % http://orcid.org/0000-0001-5346-9310
\author[9]{Marie E. Rognes}    %
\author[10]{Garth N. Wells}    % http://orcid.org/0000-0001-5291-7951

\affil[1]{Simula Research Laboratory, \texttt{martinal@simula.no}}
\affil[2]{Charles University in Prague, \texttt{blechta@karlin.mff.cuni.cz}}
\affil[3]{Simula Research Laboratory, \texttt{hake@simula.no}}
\affil[4]{Simula Research Laboratory, \texttt{august@simula.no}}
\affil[5]{Simula Research Laboratory, \texttt{benjamik@simula.no}}
\affil[6]{Chalmers University of Technology and University of Gothenburg, \texttt{logg@chalmers.se}}
\affil[7]{University of Cambridge, \texttt{chris@bpi.cam.ac.uk}}
\affil[8]{Simula Research Laboratory, \texttt{johannr@simula.no}}
\affil[9]{Simula Research Laboratory, \texttt{meg@simula.no}}
\affil[10]{University of Cambridge, \texttt{gnw20@cam.ac.uk}}

% Subject classification: 65M60

\runningtitle{The FEniCS Project Version \fenicsversion{}}
\runningauthor{}

%------------------------------------------------------------------------------
\begin{document}
\fixme{Anders: get rid of ANS style if this paper is self published?}

\maketitle
%------------------------------------------------------------------------------
\begin{abstract}
  The FEniCS Project is a collaborative project for the development of
  innovative concepts and tools for automated scientific computing,
  with a particular focus on the solution of differential equations by
  finite element methods. The FEniCS Project consists of a
  collection of interoperable software components, including DOLFIN,
  FFC, FIAT, Instant, mshr, and UFL. This note describes the new
  features and changes introduced in the release of FEniCS
  version~\fenicsversion{}.
\end{abstract}

%------------------------------------------------------------------------------
\section{Overview}

FEniCS version \fenicsversion{} was released on 28th~July 2015.
This article provides an overview of the new features of this release
and serves as a citable reference for FEniCS
version~\fenicsversion{}. The FEniCS software is available under the
GNU Lesser General Public License (LGPL) and can be downloaded from
the FEniCS Project web page at \url{http://fenicsproject.org}.

The FEniCS software consists of a collection of interoperable
components. These are, in alphabetical order, DOLFIN, FFC, FIAT,
Instant, mshr, and UFL. In addition, FEniCS includes the code
generation interface component UFC, which is now technically a part of
FFC. All FEniCS components are licensed under the GNU LGPL.

\fixme{Martin: following paragraph is from 1.5 release notes. Keep it here?}
The components are released simultaneously with the same major and
minor version numbers, making it easy to find compatible versions of
the components. This release numbering scheme is practical because the
components are developed in concert based on the principles of
continuous integration with a shared buildbot infrastructure. Thus any
change in one component that touches the interface between components
must be matched by a simultaneous change in other components, avoiding
components drifting apart. In addition, components are initially
pushed to an integration branch called ``next'', to be tested on build
servers with multiple hardware and OS combinations, before being
merged into ``master'' only if all tests pass. This is a
simplification of the ``gitworkflows'' model.  A significant effect of
this development scheme is that downloading the most recent
development versions of all components simultaneously will result in a
tested combination which usually is close to release quality.

%------------------------------------------------------------------------------
\section{New features}

\begin{itemize}
  \item
    \fixme{Martin: new, PEP 440 conforming, version numbering scheme; maybe
    mention it in the previous paragraph about versioning}
  \item
    \fixme{Marie: trace space}
  \item
    \fixme{Marie, Larry: Regge element}
  \item
    \fixme{Martin: UFL, UFLACS changes}
  \item
    Instant: nothing happened
  \item
    \fixme{Martin: some clean-up in FFC, UFC and dofmaps}
  \item
    \fixme{Garth, Steven: local solver}
  \item
    \fixme{Garth, Chris: CSR graph}
  \item
    \fixme{Garth, Chris, Jan: Eigen backend, uBLAS removal}
  \item
    \fixme{Garth: removal of UmfpackLUSolver, CholmodCholeskySolver}
  \item
    \fixme{Garth: remove GenericVector/Matrix::data()}
  \item
    \fixme{Chris, Garth: experimental Tpetra backend}
  \item
    \fixme{Anders, August: entity collision, quadrature and multimesh}
  \item
    \fixme{Martin: facet orientation fixes}
  \item
    support PETSc 3.6, require 3.4
  \item
    \fixme{Chris: exprimental mesh hierarchy}
  \item
    \fixme{Garth: ArrayView class and its consequences
    (GenericDofMap::cell\_dofs() return type)}
  \item
    \fixme{Chris: XDMF, HDF5, TimeSeries changes}
  \item
    \fixme{Martin: interface changes to assemble() and Form()}
  \item
    \fixme{Garth: PETSc PC clean-up}
  \item
    \fixme{Garth: deprecation of XML for large files in parallel}
  \item
    \fixme{Martin: rename point integral to vertex integral}
  \item
    \fixme{Garth: new GenericDofMap::dofs(mesh, dim)}
  \item
    \fixme{Chris, Garth, Anders: development in quads and hexes?}
  \item
    \fixme{Chris: removal of Exodus}
  \item
    \fixme{Chris: deprecate non\_matching\_eval and other changes in eval}
  \item
    \fixme{Garth: deprecate PETScKrylovSolver::set\_nullspace}
  \item
    \fixme{Garth: matrix compress}
  \item
    \fixme{Anders: change Rectangle|BoxMesh interface?}
  \item
    \fixme{Benjamin: mshr changes}
  \item
    \fixme{Increase maximum Newton iterations to 50}
\end{itemize}

Timing functions have been reworked for better experience in parallel. The
function \emp{list\_timings()} is now collective and reports average values
across MPI ranks in a single table. New function \emp{dump\_timings\_to\_xml()}
has been introduced for easy storing of timings to a human/machine readable
file. For full capabalities of new interface see the \emp{timing} demo
contained in the DOLFIN source tree or relevant docstrings.
%------------------------------------------------------------------------------
\section{New demo programs}
%------------------------------------------------------------------------------
\section{Interface changes}

The following interface changes were made in the release of FEniCS
version \fenicsversion{}:
%%
\begin{itemize}
   \item It has been discovered that a generated code for a dual basis of
     enriched elements was never correctly implemented. The consequence
     is that interpolation to enriched space, projection from enriched
     space to another mesh, and using constants and expression in Dirichlet
     BCs on enriched space were not working properly in DOLFIN.

     The temporary solution was to disable a computation of a dual basis for
     enriched elements so that aforementioned operations now raise an error.
     The correct implementation is expected to appear in FEniCS 1.7 or 1.8.
     See~\url{https://bitbucket.org/fenics-project/ffc/issues/69}.

  \item Python free function \emp{homogenize()} has been deprecated in favour
    of the member function \emp{DirichletBC.homogenize()} which can be used in
    a~combination with the copy constructor to mimic behaviour of the
    deprecated function.
\end{itemize}

%------------------------------------------------------------------------------
\section{Dependencies}

FEniCS, mostly DOLFIN, depends on a number of external packages. Here
we list and describe these external packages, along with required
version numbers (when known).

Necessary dependencies:
%%
\begin{itemize}
  \item
    C++11 compiler (GCC $\geq$ 4.6 and Clang 3.6 are known to work),
    see section~\citep[section 2.11]{fenics_1_5} for details
  \item
    Boost $\geq$ 1.48; provides some low-level data structures and
    algorithms, DOF reordering and mesh coloring; required components are
    \emp{filesystem, program\_options, system, thread, iostreams, timer}
    \citep{Schling:2011:BCL:2049814}
  \item
    CMake $\geq$ 2.8; used by DOLFIN build system and optionally by
    Python JIT chain
  \item
    Eigen $\geq$ 3.2; replaces uBLAS as lightweight, sequential linear
    algebra backend
    \citep{eigenweb}
  \item
    Python 2.7 or $\geq$ 3.2; six (Python 2 and 3 compatibility tool);
    needed by FIAT, UFL, FFC, and Instant; optionally needed
    for Python interface to DOLFIN and mshr
  \item
    NumPy; Python package providing data structures and operations on
    N-dimensional arrays \citep{NumPy}
  \item
    SymPy; Python symbolic maths library, used by FIAT \citep{SymPy}
\end{itemize}
%%
Optional dependencies:
%%
\begin{itemize}
  \item
    OpenMP; supports parallel computing on shared memory systems
  \item
    MPI; supports parallel computing on distributed memory systems
  \item
    PETSc $\geq$ 3.4, $\leq$ 3.6; serves as parallel (linear) algebra backend;
    provides data structures and routines for handling parallel
    vectors, matrices, Krylov solvers, sparse direct solvers, various
    preconditioners and non-linear solvers \citep{petsc-web-page,
      petsc-user-ref, petsc-efficient}
  \item
    petsc4py $\geq$ 3.4, $\leq$ 3.6; provides Python bindings to
    PETSc~\citep{Dalcin20111124}
  \item
    SLEPc $\geq$ 3.4, $\leq$ 3.6; provides parallel eigen-problem
    solvers~\citep{Hernandez:2005:SSF}
  \item
    slepc4py $\geq$ 3.5.1, $\leq$ 3.6; provides Python bindings to SLEPc
    \citep{Dalcin20111124}
  \item
    UMFPACK; provides sequential sparse LU decomposition using
    multifrontal method for usage with Eigen backend
    \citep{Davis:2004:AUV:992200.992206}
  \item
    CHOLMOD; provides sequential sparse Cholesky decomposition using
    supernodal method for usage with Eigen backend
    \citep{Chen:2008:ACS:1391989.1391995}
  \item
    PaStiX $\geq$ 5.2.1; provides parallel (both distributed and
    threaded) sparse LU and Cholesky
    decomposition~\citep{A:LaBRI::HRR01a}
  \item
    Trilinos $\geq$ 11.0.0; provides mesh partitioning,
    coloring and new experimental distributed linear algebra
    backend Tpetra; used components \emp{Tpetra, Zoltan,
    MueLu, Amesos2, Ifpack2, Belos}
    \citep{Trilinos}
  \item
    SCOTCH; provides mesh partitioning and DOF
    reordering~\citep{C:LaBRI::chepelpmaa06}
  \item
    ParMETIS $\geq$ 4.0.2; provides mesh partitioning
    ~\citep{Karypis:1998:FHQ:305219.305248}
  \item
    SWIG $\geq$  3.0.3;
    tool needed for generating Python interface to DOLFIN and mshr
  \item
    flufl.lock; implements file locking (used by Instant) on NFS file system
  \item
    HDF5; provides parallel, scalable IO backend; needs to be built with
    parallel support~\citep{hdf5}
  \item
    zlib; reading compressed XML files and compressed VTK output
  \item
    VTK $\geq$ 5.2; provides interactive plotting in DOLFIN
  \item
    CGAL 4.6, TetGen 1.5.0; mshr backends, built automatically by
    mshr build system~\citep{cgal:eb-15a, Si:2015:TDQ:2732672.2629697}
  \item
    GMP, MPFR; arbitrary-precision and multiple-precision arithmetic libraries,
    required by mshr~\citep{GMP, Fousse:2007:MMP}
  \item
    Scipy; needed by UFL for evaluation of error and Bessel functions
  \item
    pytest; Python testing tool, needed for running unit tests
  \item
    Sphinx $\geq$ 1.1.0; enables building documentation
  \item
    Qt4; provides API for writing graphical applications
  \item
    Soya; Python 3D engine, used for plotting finite elements
\end{itemize}

%------------------------------------------------------------------------------
\section{How to cite FEniCS \fenicsversion{}}

Users of FEniCS version \fenicsversion{} are encouraged to cite this
article,
\fixme{Anders: or rather~\citep{fenics_1_5} if this one gets self published?}
in addition to one or more of the publications listed on the
FEniCS Project web site:

\begin{center}
  \url{http://fenicsproject.org/citing/}
\end{center}

%------------------------------------------------------------------------------
\section{Acknowledgments}

Although this article lists as authors only those developers who have
made significant contributions specifically to the release of
FEniCS~\fenicsversion{}, we gratefully acknowledge the contributions
by many people to the FEniCS Project, including
%%
Martin S. Alnæs,
Jan Blechta,
Juan Luis Cano Rodríguez
Patrick Farrell,
Johan Hake,
Johan Hoffman,
Johan Jansson,
Niclas Jansson,
August Johansson,
Claes Johnson,
Benjamin Kehlet,
Robert C.~Kirby,
Matthew Knepley,
Miroslav Kuchta,
Hans Petter Langtangen,
Anders Logg,
Kent-Andre Mardal,
Andre Massing,
Mikael Mortensen,
Harish Narayanan,
Chris Richardson,
Johannes Ring,
Marie E.~Rognes,
L.~Ridgway Scott,
Ola Skavhaug,
Andy Terrel,
Garth~N.~Wells, and
Kristian Ølgaard.
\fixme{Someone new or forgotten to get credit? Please, shout!}

The FEniCS Project is supported by The Research Council of Norway
through a Centre of Excellence grant to the Center for Biomedical
Computing at Simula Research Laboratory, project number 179578.
%
Anders Logg acknowledges support by the Swedish Research Council Grant
No.\ 2014--6093.
%
Jan Blechta acknowledges the support of the project LL1202 in the
programme ERC-CZ funded by the Ministry of Education, Youth and Sports
of the Czech Republic.
%------------------------------------------------------------------------------
\bibliographystyle{abbrvnat}
\bibliography{bibliography}
%------------------------------------------------------------------------------
\end{document}
%------------------------------------------------------------------------------
